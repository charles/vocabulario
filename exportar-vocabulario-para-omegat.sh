#!/usr/bin/env bash
### -----------------------------------------------------------------------
### Script    : exportar-vocabulario-para-omegat.sh
### Data      : 22-12-2020, atualizado 22-04-2021
### Autor(a)  : Equipe de tradução Debian pt_BR, Thiago Pezzo (tico)
###				debian-l10n-portuguese@lists.debian.org
###				https://wiki.debian.org/Brasil/Traduzir
###				https://salsa.debian.org/l10n-br-team/vocabulario
### Licença   : GNU/GPL v3.0
###
### Descrição : Exporta o arquivo genérico de vocabulário para o formato
###				de glossário usado pelo software de tradução OmegaT
###				https://omegat.sourceforge.io/manual-standard/pt_BR/chapter.glossaries.html#d0e8525
###
### Uso		  : Executar no diretório do arquivo 'vocabulario-pt_BR.md',
###				criará um novo arquivo 'glossary.txt' que deve ser
###				copiado para o diretório correspondente do OmegaT
###
### Formato do vocabulário padrão:
### 	Termo em inglês | Tradução | Exemplos de uso e comentários
###
### Formato do glossário OmegaT
### 	Termo em inglêsTABULAÇÃOTraduçãoTABULAÇÃOInformações extras
### -----------------------------------------------------------------------

# Definições
vocabulario="vocabulario-pt_BR.md"  # vocabulário construído pela equipe
glossario="glossary.txt"					  # arquivo do OmegaT
separador="[[:blank:]]|"					  # um espaço antes do separador
tabulacao="$(printf '\t')"				  # código de tabulação
contador=0									        # contador de termos

# Mensagens
sucesso="
Equipe de tradução Debian pt-BR (https://l10n.debian.org.br)
Conversão terminada com sucesso.
	Entrada: $vocabulario
	Saída  : $glossario
	Termos :"
erro1="Arquivo do vocabulário padrão não encontrado ($vocabulario).\
		Está no diretório?"
erro2="Arquivo do glossário OmegaT já existe ($glossario).\
		Não vou sobrescrever."


# Verificação de arquivos
[ ! -e $vocabulario ] && { echo "$erro1" >&2; exit 1; }	# arquivo de vocabulário deve existir
[ -e $glossario ] && { echo "$erro2" >&2; exit 1; }		  # não sobrescreve glossário existente


# Conversão de formato
while read entrada; do							        # Lê cada linha do arquivo de vocabulário
	termo=${entrada//$separador/"$tabulacao"}	# Substitui todos os separadores por tabulações
 	echo "$termo" >> $glossario					      # Adiciona entrada no arquivo de saída
	((contador++))								            # Contagem de termos
done < $vocabulario								          # Passa o arquivo como entrada


# Sai com sucesso
echo -e "$sucesso $contador\n"
exit 0

