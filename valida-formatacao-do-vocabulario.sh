#!/usr/bin/env bash
### -----------------------------------------------------------------------------
### Script    : valida-formatacao-do-vocabulario.sh
### Data      : 22-12-2020, atualizado 07-02-2022
### Autor(a)  : Equipe de tradução Debian pt_BR, Thiago Pezzo (tico)
###             debian-l10n-portuguese@lists.debian.org
###             https://wiki.debian.org/Brasil/Traduzir
###             https://salsa.debian.org/l10n-br-team/vocabulario
### Licença   : GNU/GPL v3.0
###
### Descrição : Verifica a padronização do arquivo 'vocabulario-pt_BR.md'
###
### Uso       : Executar no diretório do arquivo 'vocabulario-pt_BR.md'
###
### Formato do vocabulário padrão:
###     Termo em inglês | Tradução | Exemplos de uso e comentários (ou em branco)
### -----------------------------------------------------------------------------

# Definições
vocabulario="vocabulario-pt_BR.md"
n=0

# Mensagens
sucesso="
Equipe de tradução Debian pt-BR (https://l10n.debian.org.br)
O vocabulário está com a formatação padrão.
    Entrada: $vocabulario
    Termos :"

erro1="
Arquivo do vocabulário padrão não encontrado ($vocabulario).
 Está no diretório correto?"

erro2="
Parece que o arquivo está fora do padrão. Por favor, verifique.
   Formato: Termo em inglês | Tradução | Exemplos de uso e comentários (ou em branco)
   Linha  :"

# Verifica se o vocabulário está no diretório
[ ! -e $vocabulario ] && { echo "$erro1" >&2; exit 1; }

# Verificação da integridade do vocabulário
# Lê cada linha do arquivo
while read entrada; do
    # Contagem de termos
    ((n++))
    # Substitui tudo que não '|' por nada
    entrada=${entrada//[!|]/}
    # Se não houver dois '|', está fora de padrão
    [[ ${#entrada} -ne 2 ]] && { echo "$erro2 $n" >&2; exit 1; }
    # Arquivo de vocabulário como entrada
done < "$vocabulario"

# Sai com sucesso
echo -e "$sucesso $n\n" >&2
exit 0

