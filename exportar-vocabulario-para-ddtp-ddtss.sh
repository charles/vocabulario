#!/usr/bin/env bash
### -----------------------------------------------------------------------
### Script    : exportar-vocabulario-para-ddtp-ddtss.sh
### Data      : 06-12-2020, atualizado 22-04-2021
### Autor(a)  : Equipe de tradução Debian pt_BR, Thiago Pezzo (tico)
###				debian-l10n-portuguese@lists.debian.org
###				https://wiki.debian.org/Brasil/Traduzir
###				https://salsa.debian.org/l10n-br-team/vocabulario
### Licença   : GNU/GPL v3.0
###
### Descrição : Exporta o arquivo genérico de vocabulário para o formato
###				usado pelo sistema de tradução de descrição de pacotes
###				DDTP/DDTSS
###
### Uso		  : Executar no diretório do arquivo 'vocabulario-pt_BR.md',
###				criará um novo arquivo 'words-pt_BR.txt' que pode ser
###				copiado diretamente para o repositório do DDTP/DDTSS
###
### Formato do vocabulário padrão:
### 	Termo em inglês | Tradução | Exemplos de uso e comentários
###
### Formato do vocabulário DDTP/DDTSS:
### 	Termo em inglêsTABULAÇÃOTradução | Exemplos de uso e comentários
### -----------------------------------------------------------------------

# Definições
vocabulario="vocabulario-pt_BR.md"
wordlist="words-pt_BR.txt"
separador=" | "
n=0

# Mensagens
sucesso="
Equipe de tradução Debian pt-BR (https://l10n.debian.org.br)
Conversão terminada com sucesso.
	Entrada: $vocabulario
	Saída  : $wordlist
	Termos :"
erro1="Arquivo do vocabulário padrão não encontrado ($vocabulario).\
		Está no diretório?"
erro2="Arquivo do vocabulário DDTP/DDTSS já existe ($wordlist).\
		Não vou sobrescrever."

# Verificação de arquivos
[ ! -e $vocabulario ] && { echo "$erro1" >&2; exit 1; }	# arquivo vocabulário deve existir
[ -e $wordlist ] && { echo "$erro2" >&2; exit 1; }		  # não sobrescreve wordlist existente

# Conversão de formato
while read entrada; do							        # Lê cada linha do arquivo
	termo=${entrada%%$separador*}				      # Separa o termo
 	traducao=${entrada#*$separador}				    # Separa a tradução
	echo -e "$termo\t$traducao" >> $wordlist	# Insere tabulação e grava
	((n++))										                # Contagem de termos
done < $vocabulario

# Sai com sucesso
echo -e "$sucesso $n\n" >&2
exit 0
